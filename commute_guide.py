#!/usr/bin/env python
import zeep
import json
import logging
import csv
import boto3
import datetime
import requests
import re
import sys
import yaml

from html.parser import HTMLParser
from flask import Flask
from flask_ask import Ask, statement, session, question

with open(r'config.yaml') as file:
    config = yaml.full_load(file)

app = Flask(__name__)
ask = Ask(app, '/')
log = logging.getLogger()
now = datetime.datetime.now()

# If Development
if config["development"]["production"] == True:
	app.config['ASK_VERIFY_REQUESTS'] = False
	app.config['ASK_APPLICATION_ID'] = ''
	logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
else:
	app.config['ASK_VERIFY_REQUESTS'] = True
	app.config['ASK_APPLICATION_ID'] = config["development"]["devappid"]

@ask.launch
def launch():
	return question("<speak><p>Hi!</p><p>Welcome to Commute Guide, what would you like to do?</p></speak>") \
		.reprompt("I didn't get that. What would you like to do?")

@ask.on_session_started
def new_session():
	log.info('[{}]: Skill Started'.format(session.user.userId[216:]))
	set_user_last_query_time(session.user.userId)
	update_times_run(session.user.userId)

@ask.session_ended
def session_ended():
	return "{}", 200

@ask.intent("TrainsIntent")
def gettrains(fr,to):
	log.info("[{}]: Starting TrainsIntent, From: {}, To: {}".format(session.user.userId[216:],fr,to))
	start = fr
	end = to
	if fr == None or to == None or fr == "" or to == "":
		log.info("[{}]: Looking for favourite".format(session.user.userId[216:]))
		start, end = get_user_stations(session.user.userId)
	else:
		if len(fr) > 3:
			start = find_station3l(fr)
		if len(to) > 3:
			end = find_station3l(to)

# No results found from database
	if start == None or start == "":
		log.info("[{}]: Couldn't find starting train station".format(session.user.userId[216:]))
		return statement("I can't find train schedule without knowing start station.")
	if end == None or end == "":
		log.info("[{}]: Couldn't find destination train station".format(session.user.userId[216:]))
		return statement("I can't find train schedule without knowing destination station.")

# More then one result found, return user list of possible choices
	if type(start) == list:
		stations = ', '.join(map(str, start))

		log.info("[{}]: Found following result(s) for start train station: {}".format(session.user.userId[216:],stations))

		if len(start) == 1:
			return statement("I have found {} instead of {}. Please try again.".format(stations,fr))
		else:
			return statement("I have found more results for {}. They are: {}. Please try again.".format(fr,stations))

	if type(end) == list:
		stations = ', '.join(map(str, end))
		log.info("[{}]: Found following result(s) for destination train station: {}".format(session.user.userId[216:],stations))

		if len(end) == 1:
			return statement("I have found {} instead of {}. Please try again.".format(stations, to))
		else:
			return statement("I have found more results for {}. They are: {}. Please try again.".format(to, stations))

	log.info("[{}]: Got start,end. Now querying NationalRail for Start: {}, End: {}".format(session.user.userId[216:],start,end))
	wsdl = 'https://lite.realtime.nationalrail.co.uk/OpenLDBWS/wsdl.aspx'
	client = zeep.Client(wsdl=wsdl)

	key_type = client.get_type('ns0:AccessToken')
	key = key_type(TokenValue=config["nationalrail"]["accesstoken"])
	log.info("[{}]: Start station: {}, End station: {}".format(session.user.userId[216:],str(start).upper(),str(end).upper()))

	result = client.service.GetArrivalDepartureBoard(numRows=5, crs=start.upper(), filterCrs=end.upper(), filterType='to', timeOffset=0, timeWindow=120, _soapheaders={'AccessToken': key})

	if result:
		log.info("[{}]: Got valid result from OpenLDBWS.".format(session.user.userId[216:]))
		objects = zeep.helpers.serialize_object(result)
	
		speech  = "<speak><p>Following trains are scheduled from " + find_stationf(start).title() + " to " + find_stationf(end).title() + "</p>\n"
		card = "Following trains are scheduled from " + find_stationf(start).title() + " to " + find_stationf(end).title() + ":\n"
	
		if objects['trainServices']:
			for k in objects['trainServices']['service']:
				speech += "<p>"
				if k['isCancelled'] == True:
					if k['cancelReason'] != None:
						speech += k['std'] + ' is cancelled ' + k['cancelReason'] + "</p>\n"
						card += k['std'] + ' is cancelled ' + k['cancelReason'] + "\n"
					else:
						speech += k['std'] + " is cancelled</p>\n"
						card += k['std'] + " is cancelled\n"
						continue
				else:
					if k['eta'] == 'On time':
						if k['sta'] != None:
							card   += k['sta']
							speech += k['sta']
						else:
							card   += k['std']
							speech += k['std']
					else:   
						if k['eta'] != None:
							card   += "Estimated train arrival: " + k['eta']
							speech += "Estimated train arrival: " + k['eta']
						if k['delayReason'] != None:
							card   += " " + k['delayReason']
							speech += " " + k['delayReason']
						else:
							if k['std'] != None:
								card   += "Estimated train departure: " + k['std']
								speech += "Estimated train departure: " + k['std']
							if k['delayReason'] != None:
								card   += " " + k['delayReason']
								speech += " " + k['delayReason']
							else:
								card   += "Estimated train departure: " + k['etd']
								speech += "Estimated train departure: " + k['etd']
							
					if k['delayReason'] != None:
						card   += " " + k['delayReason']
						speech += " " + k['delayReason']					
					if k['length']:
						card   += ", formed of " + str(k['length']) + " coaches"
						speech += ", formed of " + str(k['length']) + " coaches"					
					if k['platform']:
						card   += ", arriving on platform " + str(k['platform'])
						speech += ", arriving on platform " + str(k['platform'])
		
				speech += "</p>" + "\n"
				card += "\n"
		else:
			speech = "<speak>There are no trains scheduled for the next two hours from " + find_stationf(start).title() + " to " + find_stationf(end).title()
			card = "There are no trains scheduled for the next two hours from " + find_stationf(start).title() + " to " + find_stationf(end).title() + "\n"
	else:
		log.info("[{}]: Didn't get valid result from OpenLDBWS.".format(session.user.userId[216:]))
		speech = "<speak>I couldn't get valid information from National Rail database."
		card   = "I couldn't get valid information from National Rail database."
	
	speech += "</speak>"

	log.info('[{}]: speech = {}'.format(session.user.userId[216:],speech))
	log.info('[{}]: card = {}'.format(session.user.userId[216:],card))

	return statement(speech).simple_card(title="Commute Guide", content=card)

@ask.intent("FavouriteIntent")
def setfavourite(fr,to):
	log.info('[{}]: Trying to set favourite journey.'.format(session.user.userId[216:]))

	start = None
	end = None

	if fr != None:
		start = find_station3l(fr)
	if to != None:
		end = find_station3l(to)

	if type(start) == list:
		stations = ', '.join(map(str, start))
		log.info("[{}]: Found more then one result for start train station: {}".format(session.user.userId[216:],stations))
		return statement("I have found more results for the starting stations. They are: {}".format(stations))
	if type(end) == list:
		stations = ', '.join(map(str, end))
		log.info("[{}]: Found more then one result for end train station: {}".format(session.user.userId[216:],stations))
		return statement("I have found more results for the destination station. They are: {}".format(stations))

	if start == None or start == "":
		log.info("[{}]: I couldn't save your favourite journey because starting train station wasn't specified or was empty.".format(session.user.userId[216:]))
		reason = "I couldn't save your favourite journey because starting train station wasn't specified or was empty."
		return statement(reason).simple_card(title="Commute Guide", content=reason)

	if end == None or end == "":
		log.info("[{}]: I couldn't save your favourite journey because destination train station wasn't specified or was empty.".format(session.user.userId[216:]))
		reason = "I couldn't save your favourite journey because destination train station wasn't specified or was empty."
		return statement(reason).simple_card(title="Commute Guide", content=reason)

	set_user_stations(session.user.userId,find_station3l(fr),find_station3l(to))

	reason = "I have saved your favourite journey from: " + fr.title() + " to: " + to.title()
	return statement(reason).simple_card(title="Commute Guide", content=reason)

@ask.intent("AMAZON.HelpIntent")
def speakhelp():
	x = "To ask me for a train say: Alexa, get the trains from London Victoria to Balham. To set your favourite journey say: Alexa, save my favourite journey from Balham to London Victoria. To get your favourite journey say: Alexa, get the trains. To ask about air pollution say: Alexa, what's the air like? What can I do for you?"
	return question(x) \
		.reprompt("I didn't get that. What would you like to do?")

@ask.intent("AMAZON.CancelIntent")
def cancelfunction():
	return statement("Goodbye!")

@ask.intent("AMAZON.StopIntent")
def cancelfunction():
	return statement("Goodbye!")

def find_stationf(station):
	with open('station_codes.csv') as csvfile:
		readCSV = csv.reader(csvfile, delimiter=',')
		for row in readCSV:
			if station.lower() == row[1].lower():
				if row[0].upper() != None:
					return(row[0].upper())

def find_station3l(station):
	found = None
	arr = []
	log.info("[{}]: Trying to search for: {}".format(session.user.userId[216:],station))
	with open('station_codes.csv') as csvfile:
		readCSV = csv.reader(csvfile, delimiter=',')
		for row in readCSV:
			if station.lower() == row[0].lower():
				if row[1].upper() != None:
					found = row[1].upper()
			if re.search(".*" + station.lower() + ".*", row[0].lower()):
				arr.append(row[0])
	if found != None:
		log.info("[{}]: Found following result: {}".format(session.user.userId[216:],found))
		return found
	if len(arr) > 0:
		log.info("[{}]: Found following results: {}".format(session.user.userId[216:],arr))
		return arr

def set_user_stations(userid,start,end):
	dynamodb = boto3.resource('dynamodb', region_name='eu-west-1',
	aws_access_key_id=config["aws"]["accesskey"], aws_secret_access_key=config["aws"]["secretkey"])
	table = dynamodb.Table('favourite_trains')

	table.update_item(
		Key={
			'userid': userid,
		},
		UpdateExpression='SET start_journey = :start_journey, end_journey = :end_journey',
			ExpressionAttributeValues={
				':start_journey': start,
				':end_journey': end,
			}
		)

def set_user_last_query_time(userid):
	dynamodb = boto3.resource('dynamodb', region_name='eu-west-1',
	aws_access_key_id=config["aws"]["accesskey"], aws_secret_access_key=config["aws"]["secretkey"])
	table = dynamodb.Table('favourite_trains')

	table.update_item(
		Key={
			'userid': userid,
		},
		UpdateExpression='SET last_query_time = :querytime',
			ExpressionAttributeValues={
				':querytime': now.strftime("%Y-%m-%d %H:%M:%S"),
			}
		)

def update_times_run(userid):
	dynamodb = boto3.resource('dynamodb', region_name='eu-west-1',
	aws_access_key_id=config["aws"]["accesskey"], aws_secret_access_key=config["aws"]["secretkey"])
	table = dynamodb.Table('favourite_trains')

	response = table.get_item(
		Key={
			'userid': userid
		}
	)

	if 'Item' in response.keys():
		if 'timesrun' in response['Item']:
			timesrun = response['Item']['timesrun']
			log.info("[{}]: Times run: ".format(session.user.userId[216:],str(timesrun)))
		else:
			timesrun = 0
			log.info("[{}]: Times run: 0".format(session.user.userId[216:]))

	table.update_item(
		Key={
			'userid': userid,
		},
		UpdateExpression='SET timesrun = :timesrun',
			ExpressionAttributeValues={
				':timesrun': timesrun + 1,
			}
		)

def get_user_stations(userid):
	dynamodb = boto3.resource('dynamodb', region_name='eu-west-1',
	aws_access_key_id=config["aws"]["accesskey"], aws_secret_access_key=config["aws"]["secretkey"])
	table = dynamodb.Table('favourite_trains')

	response = table.get_item(
		Key={
			'userid': userid
		}
	)

	if 'Item' in response.keys():
		if 'start_journey' in response['Item']:
			start_journey = response['Item']['start_journey']
		else:
			start_journey = None
		if 'end_journey' in response['Item']:
			end_journey = response['Item']['end_journey']
		else:
			end_journey = None

	return start_journey,end_journey

@ask.intent("AirPollution")
def askforairpollution(when, default={'when':'today'}):
	if when == None:
		when = 'today'

	log.info("[{}]: Starting Air Pollution Intent for: {}".format(session.user.userId[216:],when))

	url = 'https://api.tfl.gov.uk/AirQuality?app_id=' + config["tflapi"]["appid"] + '&app_key=' + config["tflapi"]["appkey"]
	try:
		resp = requests.get(url=url)
	except requests.exceptions.RequestException as e:
		log.info("[{}]: Couldn't fetch data from TFL API. Following exception occured: {}".format(session.user.userId[216:],e))
		return statement("I couldn't get the information about air pollution. Please try again later.")

	if resp.status_code == 200:
		log.info("[{}]: Got 200 response code for AirPollution".format(session.user.userId[216:]))

		d = json.loads(resp.text)

		h = HTMLParser()

		weather = None

		for i in d['currentForecast']:
			if when == 'today':
				if i['forecastType'] == 'Current':
					weather = h.unescape(i['forecastText']).replace('<br/><br/>',"\n").replace('<br/>',"\n")
			if when == 'tomorrow':
				if i['forecastType'] == 'Future':
					weather = h.unescape(i['forecastText']).replace('<br/><br/>',"\n").replace('<br/>',"\n")

		if weather != None:
			log.info("[{}]: speech = {}".format(session.user.userId[216:],weather))
			return statement(weather).simple_card(title="Commute Guide", content=weather)
		else:
			log.info("[{}]: speech = I couldn't get the information about air pollution. Please try again later.".format(session.user.userId[216:]))
			return statement("I couldn't get the information about air pollution. Please try again later.")
	else:
		log.info("[{}]: speech = I couldn't get the information about air pollution. Please try again later.".format(session.user.userId[216:]))
		return statement("I couldn't get the information about air pollution. Please try again later.")

@ask.intent("TFLStatus")
def tflstatus(target, default={'target':'tube,dlr,overground,river-bus,tflrail'}):
	log.info("[{}]: Starting TFL Status Intent for {}.".format(session.user.userId[216:],target))

	if target == None:
		target = 'tube,dlr,overground,tflrail'
	if target == 'river bus':
		target == 'river-bus'

	url = 'https://api.tfl.gov.uk/Line/Mode/' + target + '/Disruption?app_id=' + config["tflapi"]["appid"] + '&app_key=' + config["tflapi"]["appkey"]
	try:
		resp = requests.get(url=url)
	except requests.exceptions.RequestException as e:
		log.info("[{}]: Couldn't fetch data from TFL API. Following exception occured: {}".format(session.user.userId[216:],e))
		return statement(err)

	if resp.status_code == 200:
		log.info("[{}]: Got succesful response from TFL".format(session.user.userId[216:]))

		def uniqlist(seq):
			seen = set()
			seen_add = seen.add
			return [x for x in seq if not (x in seen or seen_add(x))]

		d = json.loads(resp.text)

		descriptions = []

		for i in d:
			descriptions.append(i['description'])

		desc = uniqlist(descriptions)

		if len(desc) > 0:
			problemsonlines = "\n".join(map(str, desc))
			log.info('[{}]: speech = {}'.format(session.user.userId[216:],problemsonlines))
			return statement(problemsonlines).simple_card(title="Commute Guide", content=problemsonlines)
		else:
			log.info('[{}]: speech = TFL is not reporting any problems.'.format(session.user.userId[216:]))
			return statement("TFL is not reporting any problems.")
	else:
		log.info("[{}]: speech = I couldn't get the information about TFL status. Please try again later.".format(session.user.userId[216:]))
		return statement("I couldn't get the information about TFL status. Please try again later.")

if config["development"] == True:
	if __name__ == '__main__':
	    app.run(debug=True)
